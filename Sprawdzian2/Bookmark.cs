﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprawdzian2
{
    class Bookmark
    {
        public string BookmarkName { get; set; }
        public string WebAdress { get; set; }
        public static string PathToFile { get; set; }

    }
}
