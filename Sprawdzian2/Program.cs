﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprawdzian2
{
    class Program
    {
        public static void Serialize(List<Bookmark> bookmarks)
        {
            JsonSerializer jsonSerializer = new JsonSerializer();

            using (StreamWriter stream = new StreamWriter(Bookmark.PathToFile))
            {
                jsonSerializer.Serialize(stream, bookmarks);
            }

        }

        static void Main(string[] args)
        {
            bool DoContinue = true;
            string choice = "";
            List<Bookmark> BookmarkList = new List<Bookmark>();

            Console.WriteLine("Witaj w programie Bookmarks, jaką czynność chcesz wykonać?");

            do
            {
                Console.WriteLine("1.Dodaj zakładkę " +
                "\n2.Zapisz zakładki" +
                "\n3.Wczytaj zakładkę" +
                "\n4.Zakończ program");
                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        Bookmark bookmark = new Bookmark();

                        Console.WriteLine("Podaj nazwę zakładki:");
                        bookmark.BookmarkName = Console.ReadLine();

                        Console.WriteLine("Podaj adres strony internetowej do zakładki:");
                        bookmark.WebAdress = Console.ReadLine();

                        BookmarkList.Add(bookmark);
                        break;

                    case "2":
                        Console.WriteLine("Podaj ścieżkę zapisu dla zakładek:");
                        Bookmark.PathToFile = Console.ReadLine();

                        System.IO.FileInfo file = new System.IO.FileInfo(Bookmark.PathToFile);
                        file.Directory.Create();

                        Serialize(BookmarkList);
                        break;

                    case "3":
                        Bookmark bookmarkForWeb = new Bookmark();
                        Console.WriteLine("Podaj nazwę zakładki do uruchomienia:");
                        bookmarkForWeb = BookmarkList.Find(item => item.BookmarkName == Console.ReadLine());

                        if (bookmarkForWeb == null)
                        {
                            throw new Exception("Podana zakładka nie istnieje");
                        }

                        System.Diagnostics.Process.Start(bookmarkForWeb.WebAdress);
                        break;

                    case "4":
                        DoContinue = false;
                        break;
                    default:
                        Console.WriteLine("Nieprawidłowy indeks, spróbuj ponownie");
                        break;
                }
            } while (DoContinue);
        }
    }
}
